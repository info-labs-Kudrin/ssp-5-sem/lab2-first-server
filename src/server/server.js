import { fileURLToPath } from 'url';
import path from 'path';
import express from 'express';
import mysql from 'mysql';
import bodyParser from 'body-parser';

const conn = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "root",
    database: 'sspdb'
});

conn.connect(err => {
    if (err) throw err;
    console.log("Successfully connected to mysql!");
});

const PORT = 3000;

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const app = express();
// app.use(express.urlencoded({ extended: false }));
app.use(bodyParser.text({ extended: false }));
app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:5173');
    next();
});

app.get('/sum', (req, res) => {
    res.send((Number(req.query.a) + Number(req.query.b)).toString());
});

app.get('/mysql', (req, res) => {
    conn.query('SELECT * FROM Finansy', (err, resp) => {
        if (err) throw err;
        res.send(resp);
    });
})

app.post('/mysql', (req, res) => {
    let body;
    try {
        body = JSON.parse(req.body);
    }
    catch (err) {
        res.status(400).send('Отправлен не JSON-объект');
        return;
    }
    if (!body.Summ || !body.Dept) {
        res.status(400).send('Отправлен невалидный объект');
        console.log(body);
        return;
    }
    conn.query(`INSERT INTO Finansy(Dept, Summ) VALUES(${body.Dept}, ${body.Summ})`, (err, resp) => {
        if (err) console.log(err);
        res.send(resp);
    });
});

app.listen(PORT, () => {
    console.log(`Проект работает на порту ${PORT} \n`);
    console.log(`http://localhost:${PORT} \n`);
});

import './main.scss';
import code from './calculator.html?raw'

const rootEl = document.querySelector('#app')!;

rootEl.innerHTML = code;

const form = rootEl.querySelector('#sumForm');
if (form) {
  form.addEventListener('submit', e => {
    e.preventDefault();
    const fd = new FormData(e.target as HTMLFormElement);
    fetch(`http://localhost:3000/sum?a=${fd.get('a')}&b=${fd.get('b')}`, {
    })
    .then(response => response.text())
    .then((res) => {
        const inpt = rootEl.querySelector('[name=\'result\']');
        if (inpt) (inpt as HTMLInputElement).value = res;
    })
  })
}

const table = rootEl.querySelector('#mysql-table') as HTMLTableElement | null;
const tblUpd = rootEl.querySelector('#table-update');
if (tblUpd) {
  tblUpd.addEventListener('click', () => {
    // const sentData = new FormData();
    // sentData.append('a', 'b');
    fetch('http://localhost:3000/mysql', {
      method: 'GET',
    })
    .then(res => res.json())
    .then(res => {
      if (table) {
        while (table.rows.length > 1) table.deleteRow(1);
        res.forEach((obj: { idf: number, Dept: string, Summ: number} ) => {
          console.log(obj);
          const rw = table.insertRow();
          const cell1 = rw.insertCell();
          cell1.textContent = obj.idf.toString();
          const cell2 = rw.insertCell();
          cell2.textContent = obj.Dept;
          const cell3 = rw.insertCell();
          cell3.textContent = obj.Summ.toString();
        });
      }
    })
  })
}

const insertForm = rootEl.querySelector('#insertForm');
if (insertForm) {
  insertForm.addEventListener('submit', e => {
    e.preventDefault();
    const fd = new FormData(insertForm as HTMLFormElement);
    const obj: Record<string, any> = {};
    fd.forEach((val, key) => {
      obj[key] = val;
    })
    fetch(`http://localhost:3000/mysql`, {
      method: 'POST',
      body: JSON.stringify(obj),
    })
    .then(response => response.text())
    .then((res) => {
      console.log(res);
    })
  })
}
